/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.una.pol.simulador.eon.utils;
import Metricas.Metricas;
import java.io.IOException;
import java.math.BigDecimal;
import org.jgrapht.Graph;
import py.una.pol.simulador.eon.models.Demand;
import py.una.pol.simulador.eon.models.Link;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import java.util.ArrayList;
import java.util.List;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.EdgeReversedGraph;
import org.jgrapht.graph.SimpleWeightedGraph;
import py.una.pol.simulador.eon.models.Copia;
import py.una.pol.simulador.eon.models.EstablishedRoute;
import py.una.pol.simulador.eon.models.enums.TopologiesEnum;
import py.una.pol.simulador.eon.rsa.Algorithms;
import static py.una.pol.simulador.eon.utils.Utils.compararRutas;
import static py.una.pol.simulador.eon.utils.Utils.copiarGrafo;
import static py.una.pol.simulador.eon.utils.Utils.elegirRuta;
import static py.una.pol.simulador.eon.utils.Utils.isInList;
import static py.una.pol.simulador.eon.utils.Utils.ordenarProbabilidad;

/**
 *
 * @author Rafa
 */
public class Cromosoma {
    
    ArrayList<Integer> listaGenes ;
    HashMap<Integer, Integer> genesMap ;//solo para controlar que los genes creados no sean repetidos
    String seLogroMejora;
    Integer nroReconf;
    double mejora;
    Graph<Integer, Link> grafoMejor;
    ArrayList<Integer> indexOrden = new ArrayList<>();
    ArrayList<EstablishedRoute> resultadosActualElegidas = new ArrayList<>();
    ArrayList<EstablishedRoute> resultadosMejor = new ArrayList<>(); //arrayList que guarda el mejor conjunto de resultados
    ArrayList<GraphPath<Integer, Link>> rutasMejor = new ArrayList<>(); //arrayList que guarda el mejor conjunto de resultado
    ArrayList<Integer> indicesMejor = new ArrayList<>(); //arrayList que 
    int cantidadRutasMejor=0;
    public Cromosoma(){
     listaGenes = new ArrayList<>();
     genesMap = new HashMap<>();//solo para controlar que los genes creados no sean repetidos 
     nroReconf=0;
     seLogroMejora ="NO";
    }
//metodo que genera el 70% de los individuos de forma aleatoria
//se elige el objetivo AG, se calcula el bfr
//se elige los indices de las rutas elegidas y se pone en indicesElegidas y se guarda las rutas elegidas
//se desasigna las rutas elegidas con el metodo desasignarFS_DefragProAct
//Se ordena las rutas elegidas por las hormigas para su posterior re-ruteo 
 //por orden decreciente de cantidad de FS requeridos 
//volver a rutear con las nuevas condiciones mismo algoritmo
//se realiza el ruteo nuevamente
//asignarFS_Defrag
//se calcula la mejoraActual con el bfrNuevo y el bfrviejo ya con el nuevo ruteo
//Si mejoraActual es mayor a 0 quiere decir que no hay bloqueo y se agrega indicesElegidas a listaGenes formando asi el primer cromosoma 
   public Cromosoma solucionesCandidatas(List<GraphPath<Integer, Link>> rutas,Graph<Integer, Link> topologia,int capacidadFSporEnlace,ArrayList<EstablishedRoute> resultados, String algoritmoAejecutar,String objetivoAG,int porcentajeLong, Integer CantCores, BigDecimal MaxCrosstalk, Double crosstalkPerUnitLength, TopologiesEnum topology, BigDecimal fsWidth) throws IOException{
     
        boolean porBfr = false,porMsi = false;
        double bfrGrafo = 0,msiGrafo=0;
        double mejoraActual;
        EstablishedRoute rparcial;
        //GrafoMatriz copiaGrafo =  new GrafoMatriz(G.getCantidadDeVertices());
        //copiaGrafo.insertarDatos(topologia);
        
        //Copia Backup =  new Copia(topologia);
        //Graph<Integer, Link> copiaGrafo;
        
        Graph<Integer, Link> copiaGrafo = Utils.createTopology(topology,
                            CantCores,fsWidth , capacidadFSporEnlace);
        
        ArrayList<Integer> indexOrden = new ArrayList<>();
        Cromosoma cr = new Cromosoma();
        int cantReruteosIguales = 0; //para sumar la cantidad de reruteos que quedaron en con los mismos caminos (enlaces y FS)
        List<GraphPath<Integer, Link>> rutasElegidas = new ArrayList<>();;  //guarda las rutas elegidas por una hormiga
        ArrayList<Integer> indicesElegidas = new ArrayList<>(); //guarda los indices de las rutas elegidas en el vector rutas
     
        
        for (int i =0; i<rutas.size() ; i++){
            indexOrden.add(i);
        }
        //se calcula la longitud del cromosoma
        int longCromosoma = Math.round((rutas.size()*porcentajeLong)/100);
        //Selecciona el objetivo del algoritmo ACO
        switch (objetivoAG) {
           case "BFR":
            porBfr = true;
            bfrGrafo = Metricas.BFR(topologia, capacidadFSporEnlace,CantCores);
            break;
            /*
           case "MSI":
            porBfr = false;
            porMsi = true;
            msiGrafo = Metricas.MSI(G, capacidadFSporEnlace);
            break;
            */
        }

        rutasElegidas.clear();
        indicesElegidas.clear();
        mejoraActual=0;
  
        for(int i=0;i<longCromosoma;i++){
           indicesElegidas.add(indexOrden.get(elegirRutaAG(indicesElegidas,rutas.size())));
           rutasElegidas.add(rutas.get(indicesElegidas.get(i))); 
        }
        
        //while(mejoraActual<mejora && cont<rutas.size()){
           cantReruteosIguales = 0;
           //Crear la copia del grafo original manualmente
           copiarGrafo(copiaGrafo, topologia, capacidadFSporEnlace);
           //copiaGrafo=Backup.getGraph();
           for(int j=0; j<rutasElegidas.size();j++){
               
               Utils.deallocateFs(copiaGrafo,resultados.get(indicesElegidas.get(j)), crosstalkPerUnitLength);
             //Utils.assignFs(topologia, resultados.get(indicesElegidas.get(j)), crosstalkPerUnitLength);
           }
          // desasignarFS_DefragProAct(rutasElegidas, resultados, copiaGrafo, indicesElegidas); //desasignamos los FS de las rutas a reconfigurar                
            //ORDENAR LISTA
                //Metodo que ordena las rutas elegidas por las hormigas para su posterior re-ruteo 
                 //por orden decreciente de cantidad de FS requeridos
           if (rutasElegidas.size()>1){
            ordenarRutas(resultados, rutasElegidas, indicesElegidas, rutasElegidas.size());
           }
           //volver a rutear con las nuevas condiciones mismo algoritmo
           int contBloqueos =0;
           for (int i=0; i<rutasElegidas.size(); i++){
            int fs = resultados.get(indicesElegidas.get(i)).getFsWidth();
            fs++;
            
            
            //ver esta parte, no se si esta bien
            int tVida= resultados.get(indicesElegidas.get(i)).getLifetime();
            int insertionTime=resultados.get(indicesElegidas.get(i)).getInsertionTime();
            int idDemanda= resultados.get(indicesElegidas.get(i)).getId();
            // int tVida = G.acceder(rutas.get(indicesElegidas.get(i)).getInicio().getDato(),rutas.get(indicesElegidas.get(i)).getInicio().getSiguiente().getDato()).getFS()[resultados.get(indicesElegidas.get(i)).getInicio()].getTiempo();
            //Demanda demandaActual = new Demanda(rutasElegidas.get(i).getInicio().getDato(), obtenerFin(rutasElegidas.get(i).getInicio()).getDato(), fs, tVida);
            Demand demandaActual= new Demand(idDemanda, resultados.get(i).getFrom(), resultados.get(i).getTo(), fs, tVida, false, insertionTime);
    //        GraphPath<Integer, Link> ksp = listaKSP.get(indicesElegidas.get(i));
            // GraphPath<Integer, Link> ksp = resultados.get(indicesElegidas.get(i)).getListaKSP().get(i);
            //rparcial = realizarRuteo(algoritmoAejecutar,demandaActual,copiaGrafo, ksp,capacidadFSporEnlace);
            rparcial= Algorithms.ruteoCoreMultiple(copiaGrafo, demandaActual, capacidadFSporEnlace, CantCores, MaxCrosstalk, crosstalkPerUnitLength);
            if (rparcial != null) {
              //asignarFS_Defrag(ksp, rparcial, copiaGrafo, demandaActual, 0);
              Utils.assignFs(copiaGrafo, rparcial, crosstalkPerUnitLength);
              //verificar si eligio el mismo camino y fs para no sumar en reruteadas
              if (compararRutas(rparcial,resultados.get(indicesElegidas.get(i)))){
              cantReruteosIguales++;
              }
            } else {
                contBloqueos++;
              }
            }
            //si hubo bloqueo no debe contar como una solucion
            if(contBloqueos==0){
             mejoraActual = Utils.calculoMejoraAG(porMsi,porBfr,copiaGrafo,bfrGrafo,capacidadFSporEnlace,msiGrafo,CantCores);
            } else {
             mejoraActual = 0;
            }         
           // System.out.println("mejoraActual  : "+ mejoraActual);
            //System.out.println("bfrGrafo  : "+ bfrGrafo);
            if(mejoraActual>0){
            for(int i= 0;i<indicesElegidas.size(); i++){
              listaGenes.add(indicesElegidas.get(i));
            }
            cr.setListaGenes(listaGenes);
            cr.setNroReconf(listaGenes.size()-cantReruteosIguales);
            cr.setMejora(mejoraActual);
            cr.setSeLogroMejora("NO");
            }else{
                listaGenes.clear();
                //genesMap.clear();
                //cr.genesMap.clear();
                cr.listaGenes.clear();
            }
      return cr;
    }    
    
    //Metodo que ordena las rutas elegidas por las hormigas para su posterior re-ruteo 
    //por orden decreciente de cantidad de FS requeridos
    //Se usa en solucionesCandidatas (70%) y buenasSoluciones(30%)
    public static void ordenarRutas(ArrayList<EstablishedRoute> resultados, List<GraphPath<Integer, Link>> rutasElegidas, ArrayList<Integer> indices,int rutasElegidasSize){
        Integer aux = 0;
       // GraphPath<Integer, Link> aux2 = new SimpleWeightedGraph<>(Link.class);
       List<GraphPath<Integer, Link>> aux2 = new ArrayList<>();
        for (int i = 0; i <= rutasElegidasSize - 1; i++) {
            for (int j = i + 1; j < rutasElegidasSize; j++) {
                //Integer fin = resultados.get(indices.get(j)).getFin()-resultados.get(indices.get(j)).getInicio();
                //Integer inicio = resultados.get(indices.get(i)).getFin()-resultados.get(indices.get(i)).getInicio();
                Integer fin = resultados.get(indices.get(j)).getFsWidth();
                Integer inicio = resultados.get(indices.get(i)).getFsWidth();  
                if ( fin > inicio) {
                    //cambia el orden del array de indices
                    aux = indices.get(i);
                    indices.set(i,indices.get(j));
                    indices.set(j, aux);
                    
                    //cambia el orden en el array de rutas
                    aux2.add(rutasElegidas.get(i));
                    rutasElegidas.set(i,rutasElegidas.get(j));
                    rutasElegidas.set(j, aux2.get(0));
                }
            }
        }
    }
    
    public HashMap<Integer, Integer> getGenesMap() {
        return genesMap;
    }

    public void setGenesMap(HashMap<Integer, Integer> genesMap) {
        this.genesMap = genesMap;
    }

    public String getSeLogroMejora() {
        return seLogroMejora;
    }

    public void setSeLogroMejora(String seLogroMejora) {
        this.seLogroMejora = seLogroMejora;
    }
    
    public ArrayList<Integer> getListaGenes() {
        return listaGenes;
    }

    public void setListaGenes(ArrayList<Integer> listaGenes) {
        this.listaGenes = listaGenes;
    }

    public Integer getNroReconf() {
        return nroReconf;
    }

    public void setNroReconf(Integer nroReconf) {
        this.nroReconf = nroReconf;
    }

    public double getMejora() {
        return mejora;
    }

    public void setMejora(double mejora) {
        this.mejora = mejora;
    }

    public Graph<Integer, Link> getGrafoMejor() {
        return grafoMejor;
    }

    public void setGrafoMejor(Graph<Integer, Link> grafoMejor) {
        this.grafoMejor = grafoMejor;
    }

    public ArrayList<Integer> getIndexOrden() {
        return indexOrden;
    }

    public void setIndexOrden(ArrayList<Integer> indexOrden) {
        this.indexOrden = indexOrden;
    }

    public ArrayList<EstablishedRoute> getResultadosActualElegidas() {
        return resultadosActualElegidas;
    }

    public void setResultadosActualElegidas(ArrayList<EstablishedRoute> resultadosActualElegidas) {
        this.resultadosActualElegidas = resultadosActualElegidas;
    }

    public ArrayList<EstablishedRoute> getResultadosMejor() {
        return resultadosMejor;
    }

    public void setResultadosMejor(ArrayList<EstablishedRoute> resultadosMejor) {
        this.resultadosMejor = resultadosMejor;
    }

    public ArrayList<GraphPath<Integer, Link>> getRutasMejor() {
        return rutasMejor;
    }

    public void setRutasMejor(ArrayList<GraphPath<Integer, Link>> rutasMejor) {
        this.rutasMejor = rutasMejor;
    }

    public ArrayList<Integer> getIndicesMejor() {
        return indicesMejor;
    }

    public void setIndicesMejor(ArrayList<Integer> indicesMejor) {
        this.indicesMejor = indicesMejor;
    }

    public int getCantidadRutasMejor() {
        return cantidadRutasMejor;
    }

    public void setCantidadRutasMejor(int cantidadRutasMejor) {
        this.cantidadRutasMejor = cantidadRutasMejor;
    }
    //Metodo que genera cromosomas con el metodo de la ruleta (corresponde a la parte de 30% de la poblaciòn) "RAFA"
    //se elije el objetivoAG
    //se usa el metodo crearSolucionBFR y se obtiene un cromosoma por medio de la ruleta
    //Evaluar la solucion propuesta
    //se genera los indicesElegidas mediante la solucion creada por crearSolucionBFR y se genera las rutasElegidas
    //desasignamos los FS de las rutas a reconfigurar
    //Ordena las rutas por requerimiento de FS
    //volver a rutear con las nuevas condiciones mismo algoritmo
    public Cromosoma buenasSoluciones(List<GraphPath<Integer, Link>> rutas, Graph<Integer, Link>topologia,
        int capacidadFSporEnlace,ArrayList<EstablishedRoute> resultados, String algoritmoAejecutar,
        int porcentajeLongCRAG,String objetivoAG,int cantidadBuenasSol,ArrayList<Integer> mejorSol,int cont,Integer CantCores, BigDecimal MaxCrosstalk, Double crosstalkPerUnitLength, TopologiesEnum topology, BigDecimal fsWidth) throws IOException{
        boolean  porBfr = false, porMsi = false;
        double  bfrGrafo = 0, msiGrafo=0;
        double mejoraActual;
        double  sumBFRrutaGrafo = 0;
        double[] valorBFRrutasGrafo = new double[rutas.size()];
        
        EstablishedRoute rparcial;
        
        //Copia Backup =  new Copia(topologia);
        
        
        //GrafoMatriz copiaGrafo =  new GrafoMatriz(G.getCantidadDeVertices());
        //copiaGrafo.insertarDatos(topologia);
        
        Graph<Integer, Link> copiaGrafo = Utils.createTopology(topology,
                            CantCores,fsWidth , capacidadFSporEnlace);
        
        
        Cromosoma cr = new Cromosoma();
        int cantReruteosIguales; //para sumar la cantidad de reruteos que quedaron en con los mismos caminos (enlaces y FS)
        ArrayList<GraphPath<Integer, Link>> rutasElegidas = new ArrayList<>();  //guarda las rutas elegidas para un cromosoma
        ArrayList<Integer> indicesElegidas = new ArrayList<>(); //guarda los indices de las rutas elegidas en el vector rutas
        ArrayList<Integer> solucion = new ArrayList<Integer>();
        
        //se calcula la longitud del cromosoma
        int longCromosoma = Math.round((rutas.size()*porcentajeLongCRAG)/100);
        //Selecciona el objetivo del AG
        //switch (objetivoAG) {
         //case "BFR":
           if (objetivoAG=="BFR"){ 
             porBfr = true;
             porMsi = false;
             bfrGrafo = Metricas.BFR(topologia, capacidadFSporEnlace, CantCores);
             //sumCantLink =0;
             //System.out.println("bfr Grafo original: "+ bfrGrafo);
             /*
             for (int i =0; i<rutas.size() ; i++){
                //ver esta parte BFRdeRuta corregir
             valorBFRrutasGrafo[i] = Utils.BFRdeRuta(rutas.get(i),capacidadFSporEnlace, topologia, resultados.get(i) );//Obtengo los bfr de las rutas  
            
             sumBFRrutaGrafo = sumBFRrutaGrafo+valorBFRrutasGrafo[i];
             
             } 
            // bfrGrafo=0;
             bfrGrafo= sumBFRrutaGrafo/rutas.size();
             System.out.println("bfr grafo modificado: "+ bfrGrafo );
             
             System.out.println("prueba : " );
             */
             
             
             //break;
             /*
         case "MSI":
             porBfr = false;
             porMsi = true;
             msiGrafo = Metricas.MSI(G, capacidadFSporEnlace);
             break;
            */

             /*
         case "MSI":
             porBfr = false;
             porMsi = true;
             msiGrafo = Metricas.MSI(G, capacidadFSporEnlace);
             break;
            */
         }
        
        //Crear una buena solucion,dependiendo de la metrica estudiada
        switch (objetivoAG) {
         case "BFR":
             //se usa el metodo crearSolucionBFR y se obtiene un cromosoma por medio de la ruleta
          solucion = crearSolucionBFR(resultados,rutas,longCromosoma,cantidadBuenasSol,mejorSol,capacidadFSporEnlace,cont, topologia);
          
             //bfrGrafo = Metricas.BFR(topologia, capacidadFSporEnlace, CantCores);
             //sumCantLink =0;
           //  System.out.println("en la parte de crearsolucionBFR: "+ bfrGrafo);
          
          
          
          break;
          /*
         case "MSI":
          solucion = crearSolucionMSI(resultados,rutas,longCromosoma,cantidadBuenasSol,mejorSol,cont);
          break;
           */
        
        }
        //Evaluar la solucion propuesta
        rutasElegidas.clear();
        indicesElegidas.clear();
        mejoraActual=0;  
        for(int i=0;i<longCromosoma;i++){
           indicesElegidas.add(solucion.get(i));
           rutasElegidas.add(rutas.get(indicesElegidas.get(i))); 
        }
        cantReruteosIguales = 0;
        //Crear la copia del grafo original manualmente
        copiarGrafo(copiaGrafo, topologia, capacidadFSporEnlace);
        
        bfrGrafo = Metricas.BFR(copiaGrafo, capacidadFSporEnlace, CantCores);
             //sumCantLink =0;
        System.out.println("bfr copiaGrafo: "+ bfrGrafo);
        //Gson gson = new Gson();
        //String graphStr= gson.toJson(topologia);
        //Graph<Integer, Link> copiaGrafo = gson.fromJson(graphStr, topologia.getClass())
        //Copia Backup =  new Copia(topologia);
        //Graph<Integer, Link> copiaGraf= new SimpleWeightedGraph<>(Link.class);
        //Graph<Integer, Link> revGraph = new EdgeReversedGraph<>(topologia);
        //Graph<Integer, Link> copiaGrafo =(Graph<Integer, Link>)topologia.clone();
          //  Cloner cloner=new Cloner();
          //  Factura copia=cloner.deepClone(f);
        //revGraph= null;
        //copiaGrafo=Backup.getGraph();
        /*
              bfrGrafo = Metricas.BFR(copiaGrafo, capacidadFSporEnlace, CantCores);
             //sumCantLink =0;
             System.out.println("bfr despues de hacer la copia revGraf: "+ bfrGrafo);
        */
         
        for(int j=0; j<rutasElegidas.size();j++){
              
              Utils.deallocateFs(copiaGrafo,resultados.get(indicesElegidas.get(j)), crosstalkPerUnitLength);
             //Utils.assignFs(topologia, resultados.get(indicesElegidas.get(j)), crosstalkPerUnitLength);
           }
        /*
                     bfrGrafo = Metricas.BFR(copiaGrafo, capacidadFSporEnlace, CantCores);
             System.out.println("bfr Grafo despues de quitar: "+ bfrGrafo);
        */
//        desasignarFS_DefragProAct(rutasElegidas, resultados, copiaGrafo, indicesElegidas); //desasignamos los FS de las rutas a reconfigurar                
        //Ordena las rutas por requerimiento de FS
        if (rutasElegidas.size()>1){
         ordenarRutas(resultados, rutasElegidas, indicesElegidas, rutasElegidas.size());
        }
        
        //volver a rutear con las nuevas condiciones mismo algoritmo
        int contBloqueos =0;
        
        for (int i=0; i<rutasElegidas.size(); i++){
            int fs = resultados.get(indicesElegidas.get(i)).getFsWidth();
            //int fs = resultados.get(indicesElegidas.get(i)).getFin() - resultados.get(indicesElegidas.get(i)).getInicio();
            fs++;
            int tVida= resultados.get(indicesElegidas.get(i)).getLifetime();
            int insertionTime=resultados.get(indicesElegidas.get(i)).getInsertionTime();
            int idDemanda= resultados.get(indicesElegidas.get(i)).getId();
            
            
            //int tVida = G.acceder(rutas.get(indicesElegidas.get(i)).getInicio().getDato(),rutas.get(indicesElegidas.get(i)).getInicio().getSiguiente().getDato()).getFS()[resultados.get(indicesElegidas.get(i)).getInicio()].getTiempo();
            //Demand demandaActual = new Demand(rutasElegidas.get(i).getInicio().getDato(), obtenerFin(rutasElegidas.get(i).getInicio()).getDato(), fs, tVida);
           // Demand demandaActual= new Demand(idDemanda, rutasElegidas.get(i).getStartVertex(), rutasElegidas.get(i).getEndVertex(), fs, tVida, false, insertionTime);
             Demand demandaActual= new Demand(idDemanda, resultados.get(indicesElegidas.get(i)).getFrom(), resultados.get(indicesElegidas.get(i)).getTo(), fs, tVida, false, insertionTime);
            // GraphPath<Integer, Link> ksp = listaKSP.get(indicesElegidas.get(i));
            //GraphPath<Integer, Link> ksp = resultados.get(indicesElegidas.get(i)).getListaKSP().get(i);
            
            //ListaEnlazada[] ksp = listaKSP.get(indicesElegidas.get(i));
            rparcial= Algorithms.ruteoCoreMultiple(copiaGrafo, demandaActual, capacidadFSporEnlace, CantCores, MaxCrosstalk, crosstalkPerUnitLength);
            
            //rparcial = realizarRuteo(algoritmoAejecutar,demandaActual,copiaGrafo, ksp,capacidadFSporEnlace);
           
            if (rparcial != null) {
             
              Utils.assignFs(copiaGrafo, rparcial, crosstalkPerUnitLength);
               //asignarFS_Defrag(ksp, rparcial, copiaGrafo, demandaActual, 0);
              //verificar si eligio el mismo camino y fs para no sumar en reruteadas
              
              if (compararRutas(rparcial,resultados.get(indicesElegidas.get(i)))){  //aca revienta el codigo, verificar
                    cantReruteosIguales++;
                }
              
              bfrGrafo = Metricas.BFR(copiaGrafo, capacidadFSporEnlace, CantCores);
             System.out.println("bfr Grafo despues de poner: "+ bfrGrafo);
              
            } else {
              contBloqueos++;
            }
            
        }

       // System.out.println("TOTAL DE BLOQUEOS: " );
        //si hubo bloqueo no debe contar como una solucion
        if(contBloqueos==0){
            
         mejoraActual = Utils.calculoMejoraAG(porMsi,porBfr,copiaGrafo,bfrGrafo,capacidadFSporEnlace,msiGrafo,CantCores);
        
        } else {
         mejoraActual = 0;
        } 
        /*    
             bfrGrafo = Metricas.BFR(copiaGrafo, capacidadFSporEnlace, CantCores);
             System.out.println("bfr Grafo copia: "+ bfrGrafo);
                copiaGrafo= null;
        */
        
        if(mejoraActual>0){
            //System.out.println("prueba mejor actual: " );
            for(int i= 0;i<indicesElegidas.size(); i++){
              listaGenes.add(indicesElegidas.get(i));
            }
            cr.setListaGenes(listaGenes);
            cr.setNroReconf(listaGenes.size()-cantReruteosIguales);
            cr.setMejora(mejoraActual);
            cr.setSeLogroMejora("NO");
        }else{//si no es solucion retorna un solucion vacia
            listaGenes.clear();
            cr.listaGenes.clear();
        }
      //  System.out.println("prueba mejor actual: "+cr );
      
      return cr;
    }
    /*
   //crear una solucion teniendo en cuenta el msi
   public static ArrayList<Integer> crearSolucionMSI(ArrayList<Resultado> resultados, 
        ArrayList<ListaEnlazada> rutas,int longCromosoma,int cantSoluciones,
        ArrayList<Integer> mejorSolucion,int cont){
        double[] valorMSIrutas = new double[rutas.size()];
        ArrayList<Integer> indexOrden = new ArrayList<>();
       // ArrayList<Integer> solucion = new ArrayList<>();
        ArrayList<Integer> mejorSolucionAux = new ArrayList<>();
        ArrayList<ListaEnlazada> rutasElegidas = new ArrayList<>();
        ArrayList<Integer> indicesElegidas = new ArrayList<>();
        int rutasQueParticipan = 0;
        int aux=-1;
        double sumatoria;
        double[] probabilidad = new double[rutas.size()];
        for (int i =0; i<mejorSolucion.size() ; i++){
            mejorSolucionAux.add(mejorSolucion.get(i));
        }
        if(cont == 0){
            for (int i =0; i<rutas.size() ; i++){
             valorMSIrutas[i] = resultados.get(i).getFin();//Obtengo los msi de las rutas  
            } 
            for (int i =0; i<valorMSIrutas.length ; i++){
             indexOrden.add(i);
            }
            //ordenar vector valorMSIrutas de acuerdo a su probabilidad
            ordenarRutasMsi(valorMSIrutas, indexOrden);
            int pos = valorMSIrutas.length-1;
            while(indicesElegidas.size()<longCromosoma){
               indicesElegidas.add(indexOrden.get(pos));
               pos = pos -1;
            }
        }else{
        for (int i =0; i<rutas.size() ; i++){
         valorMSIrutas[i] = resultados.get(i).getFin();//Obtengo los msi de las rutas  
        } 
        for (int i =0; i<valorMSIrutas.length ; i++){
          indexOrden.add(i);
        }
        //calcular la probabilidad
        sumatoria=0.0;
        for(int i=0; i<rutas.size(); i++){
          sumatoria = sumatoria+valorMSIrutas[i];
        }
        //for(int i=rutasQueParticipan+1;i<rutas.size();i++){
        for(int i=0;i<rutas.size();i++){
          probabilidad[i] = (valorMSIrutas[i])/sumatoria;
        }
        //ordenar vector indice de acuerdo a su probabilidad de forma creciente y tambien se ordena a la par el indexOrden
        ordenarProbabilidad(probabilidad, indexOrden);
        for(int i=0;i<longCromosoma;i++){
           indicesElegidas.add(indexOrden.get(elegirRuta(probabilidad, indicesElegidas, indexOrden)));
        }  
        }
        return  indicesElegidas;
   }
   
   public static void ordenarRutasMsi(double[] listaRutasMsi, ArrayList<Integer> orden){
        double auxp;
        int auxi;
        int n = listaRutasMsi.length;
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (listaRutasMsi[i] > listaRutasMsi[j]) {
                    auxp = listaRutasMsi[i];
                    listaRutasMsi[i] = listaRutasMsi[j];
                    listaRutasMsi[j] = auxp;
                    
                    //cambiar el orden del vector de indices
                    auxi = orden.get(i);
                    orden.set(i,orden.get(j));
                    orden.set(j, auxi);
                }
            }
        }
    }
   */
   //Ordenar las rutas por BFR, de menor a mayor.. 
   public static void ordenarRutasBFR(double[] listaRutasBFR, ArrayList<Integer> orden){
        double auxp;
        int auxi;
        int n = listaRutasBFR.length;
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (listaRutasBFR[i] > listaRutasBFR[j]) {
                    auxp = listaRutasBFR[i];
                    listaRutasBFR[i] = listaRutasBFR[j];
                    listaRutasBFR[j] = auxp;
                    //cambiar el orden del vector de indices
                    auxi = orden.get(i);
                    orden.set(i,orden.get(j));
                    orden.set(j, auxi);
                }
            }
        }
    }
   //se usa en la parte de solucionesCandidatas(70%) para elegir los indices de las rutas a ser elegidas
    public static int elegirRutaAG(ArrayList<Integer> indices,int rutasActivas){
        Random randomGenerator = new Random();
        int indiceAleatorio = randomGenerator.nextInt(rutasActivas);
       // System.out.println("indice ALEATORIO  : " +indiceAleatorio);
        while(isInList(indices,indiceAleatorio)){ //MIENTRAS EL INDICE ESTE EN LA LISTA VA A GENERAR OTRO PORQUE SI ESTA EN LA LISTA ES REPETIDO, HACE MIENTRAS DE TRUE
          //  System.out.println("ENTRO EN EL WHILE1  : " +indiceAleatorio);
           indiceAleatorio = randomGenerator.nextInt(rutasActivas); 
          // System.out.println("ENTRO EN EL WHILE2  : " +indiceAleatorio);
        }
        return indiceAleatorio;
    }
    
     //crear una solucion teniendo en cuenta el BFR
    //se usa en la parte de buenasSoluciones (30%), usa para generar cromosomas la rulera apartir del segundo cromosoma
    //y en el primer cromosoma se obtiene el bfr de la rutas, se ordena de forma creciente y se elige los ultimos
    //la cantidad elegida equivalente al largor del cromosoma "RAFA".
   public static ArrayList<Integer> crearSolucionBFR(ArrayList<EstablishedRoute> resultados, List<GraphPath<Integer, Link>> rutas,
        int longCromosoma,int cantSoluciones,ArrayList<Integer> mejorSolucion,int capacidad,int cont, Graph<Integer, Link>topologia){
        double[] valorBFRrutas = new double[rutas.size()];
        ArrayList<Integer> indexOrden = new ArrayList<>();
        ArrayList<Integer> solucion = new ArrayList<>();
        ArrayList<Integer> mejorSolucionAux = new ArrayList<>();
        ArrayList<EstablishedRoute> rutasElegidas = new ArrayList<>();
        ArrayList<Integer> indicesElegidas = new ArrayList<>();
        int rutasQueParticipan = 0;
        int aux=-1;
        double sumatoria;
        double[] probabilidad = new double[rutas.size()];
        for (int i =0; i<mejorSolucion.size() ; i++){
            mejorSolucionAux.add(mejorSolucion.get(i));
        }
        if(cont == 0){
            for (int i =0; i<rutas.size() ; i++){
                //ver esta parte BFRdeRuta corregir
             valorBFRrutas[i] = Utils.BFRdeRuta(rutas.get(i),capacidad, topologia, resultados.get(i) );//Obtengo los bfr de las rutas  
            } 
            for (int i =0; i<valorBFRrutas.length ; i++){
             indexOrden.add(i);
            }
            
            
            
            /*
                       //calcular la probabilidad
           sumatoria=0.0;
           for(int i=0; i<rutas.size(); i++){
             sumatoria = sumatoria+valorBFRrutas[i];
           }
           //rutasQueParticipan = (rutas.size()*55)/100;//solo las 50 mejores participan en la ruleta
           for(int i=0;i<rutas.size();i++){
             probabilidad[i] = (valorBFRrutas[i])/sumatoria;
           }
            
            
           System.out.println("sumatoria"+ sumatoria);
            */
            //ordenar vector valorBFRrutas de acuerdo a su probabilidad, y tambien se ordena el indexOrden para que siga correspondiendo al vector valorBFRrutas
            ordenarRutasBFR(valorBFRrutas, indexOrden);
            int pos = valorBFRrutas.length-1;
            //elige las rutas osea genes de acuerdo al valorBFRrutas, mayor bfr son elegidos, los ultimos "RAFA"
            while(indicesElegidas.size()<longCromosoma){
               indicesElegidas.add(indexOrden.get(pos));
               pos = pos -1;
            }
            
             for (int i =0; i<rutas.size() ; i++){
                //ver esta parte BFRdeRuta corregir
             valorBFRrutas[i] = 0;  
            }
            
            
           // System.out.println("hola");
        }else{
          //  System.out.println("hola");
            for (int i =0; i<rutas.size() ; i++){
                
            valorBFRrutas[i] = Utils.BFRdeRuta(rutas.get(i),capacidad, topologia, resultados.get(i));//Obtengo los bfr de las rutas  
            } 
            for (int i =0; i<valorBFRrutas.length ; i++){
                indexOrden.add(i);
            }
           //calcular la probabilidad
           sumatoria=0.0;
           for(int i=0; i<rutas.size(); i++){
             sumatoria = sumatoria+valorBFRrutas[i];
           }
           //System.out.println("sumatoria"+ sumatoria);
            //rutasQueParticipan = (rutas.size()*55)/100;//solo las 50 mejores participan en la ruleta
           for(int i=0;i<rutas.size();i++){
             probabilidad[i] = (valorBFRrutas[i])/sumatoria;
           }
           //ordenar vector indice de acuerdo a su probabilidad de forma creciente y tambien se ordena a la par el indexOrden
           ordenarProbabilidad(probabilidad, indexOrden);
           // metodo elegirRuta realiza el metodo de la ruleta para elegir las rutas osea genes, hace hasta formar un cromosoma
           for(int i=0;i<longCromosoma;i++){
            indicesElegidas.add(indexOrden.get(elegirRuta(probabilidad, indicesElegidas, indexOrden)));
           }
              //System.out.println("indices elegidas"+indicesElegidas);
               //System.out.println("cont"+ cont);
               //System.out.println("indices elegidas"+indicesElegidas);
        }
        //retorna indicesElegidas, seria el cromosoma generada por medio de la ruleta

        return  indicesElegidas;
   }
    
    
}
